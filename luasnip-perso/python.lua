local ls = require("luasnip")
local s = ls.snippet
local t = ls.text_node
local i = ls.insert_node

ls.config.setup({ enable_autosnippets = true })

ls.add_snippets("python", {
	s({ trig = "print", snippetType = "autosnippet" }, {
		t("print("),
		i(1),
		t(")"),
	}),
})
