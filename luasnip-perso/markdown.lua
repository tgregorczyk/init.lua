local ls = require("luasnip")
local s = ls.snippet
local t = ls.text_node
local i = ls.insert_node

-- TODO: i trema et circonflexe
-- TODO: a circonflexe pour le mais
-- TODO: u circonflexe pour le cout et u accent grave pour ou
--
ls.config.setup({ enable_autosnippets = true })

ls.add_snippets("markdown", { s({ trig = "~!", snippetType = "autosnippet" }, { t("é"), i(1) }) })
ls.add_snippets("markdown", { s({ trig = "~a", snippetType = "autosnippet" }, { t("à"), i(1) }) })
ls.add_snippets("markdown", { s({ trig = "~c", snippetType = "autosnippet" }, { t("ç"), i(1) }) })
ls.add_snippets("markdown", { s({ trig = "~eg", snippetType = "autosnippet" }, { t("è"), i(1) }) })
ls.add_snippets("markdown", { s({ trig = "~ea", snippetType = "autosnippet" }, { t("é"), i(1) }) })
ls.add_snippets("markdown", { s({ trig = "~!", snippetType = "autosnippet" }, { t("é"), i(1) }) })
ls.add_snippets("markdown", { s({ trig = "~ec", snippetType = "autosnippet" }, { t("ê"), i(1) }) })
ls.add_snippets("markdown", { s({ trig = "~A", snippetType = "autosnippet" }, { t("À"), i(1) }) })
ls.add_snippets("markdown", { s({ trig = "~C", snippetType = "autosnippet" }, { t("Ç"), i(1) }) })
ls.add_snippets("markdown", { s({ trig = "~Eg", snippetType = "autosnippet" }, { t("È"), i(1) }) })
ls.add_snippets("markdown", { s({ trig = "~Ea", snippetType = "autosnippet" }, { t("É"), i(1) }) })
ls.add_snippets("markdown", { s({ trig = "~Ec", snippetType = "autosnippet" }, { t("Ê"), i(1) }) })
ls.add_snippets("markdown", { s({ trig = "~oc", snippetType = "autosnippet" }, { t("ô"), i(1) }) })
ls.add_snippets("markdown", { s({ trig = "~oe", snippetType = "autosnippet" }, { t("ö"), i(1) }) })
ls.add_snippets("markdown", { s({ trig = "~u", snippetType = "autosnippet" }, { t("û"), i(1) }) })
ls.add_snippets("markdown", { s({ trig = "~@", snippetType = "autosnippet" }, { t("·"), i(1) }) })

ls.add_snippets("markdown", { s({ trig = "Etat", snippetType = "autosnippet" }, { t("État"), i(1) }) })
