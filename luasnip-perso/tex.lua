local ls = require("luasnip")
local types = require("luasnip.util.types")
local s = ls.snippet
local t = ls.text_node
local i = ls.insert_node
local c = ls.choice_node
local d = ls.dynamic_node
local sn = ls.snippet_node

ls.config.set_config({
	history = true,
	-- Update more often, :h events for more info.
	updateevents = "TextChanged,TextChangedI",

	ext_opts = {
		[types.choiceNode] = {
			active = {
				virt_text = { { "●", "GruvboxOrange" } },
			},
			passive = {
				virt_text = { { "●", "GruvboxOrange" } },
			},
		},
	},
	enable_autosnippets = true,
})

ls.add_snippets("tex", { s({ trig = "~!", snippetType = "autosnippet" }, { t("é"), i(1) }) })
ls.add_snippets("tex", { s({ trig = "~a", snippetType = "autosnippet" }, { t("à"), i(1) }) })
ls.add_snippets("tex", { s({ trig = "~c", snippetType = "autosnippet" }, { t("ç"), i(1) }) })
ls.add_snippets("tex", { s({ trig = "~eg", snippetType = "autosnippet" }, { t("è"), i(1) }) })
ls.add_snippets("tex", { s({ trig = "~ea", snippetType = "autosnippet" }, { t("é"), i(1) }) })
ls.add_snippets("tex", { s({ trig = "~!", snippetType = "autosnippet" }, { t("é"), i(1) }) })
ls.add_snippets("tex", { s({ trig = "~ec", snippetType = "autosnippet" }, { t("ê"), i(1) }) })
ls.add_snippets("tex", { s({ trig = "~A", snippetType = "autosnippet" }, { t("À"), i(1) }) })
ls.add_snippets("tex", { s({ trig = "~C", snippetType = "autosnippet" }, { t("Ç"), i(1) }) })
ls.add_snippets("tex", { s({ trig = "~Eg", snippetType = "autosnippet" }, { t("È"), i(1) }) })
ls.add_snippets("tex", { s({ trig = "~Ea", snippetType = "autosnippet" }, { t("É"), i(1) }) })
ls.add_snippets("tex", { s({ trig = "~Ec", snippetType = "autosnippet" }, { t("Ê"), i(1) }) })
ls.add_snippets("tex", { s({ trig = "~oc", snippetType = "autosnippet" }, { t("ô"), i(1) }) })
ls.add_snippets("tex", { s({ trig = "~oe", snippetType = "autosnippet" }, { t("ö"), i(1) }) })
ls.add_snippets("tex", { s({ trig = "~u", snippetType = "autosnippet" }, { t("û"), i(1) }) })
ls.add_snippets("tex", { s({ trig = "~@", snippetType = "autosnippet" }, { t("·"), i(1) }) })

ls.add_snippets("tex", { s({ trig = "cad", snippetType = "autosnippet" }, { t("c'est-à-dire "), i(1) }) })
ls.add_snippets("tex", { s({ trig = "Etat", snippetType = "autosnippet" }, { t("État "), i(1) }) })
