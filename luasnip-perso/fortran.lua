local ls = require("luasnip")
local types = require("luasnip.util.types")
local s = ls.snippet
local t = ls.text_node
local i = ls.insert_node
local c = ls.choice_node
local d = ls.dynamic_node

ls.config.set_config({
	history = true,

	updateevents = "TextChanged,TextChangedI",

	-- works for tex.lua but not here somehow ...
	ext_opts = {
		[types.choiceNode] = {
			active = {
				virt_text = { { "●", "GruvboxOrange" } },
			},
			passive = {
				virt_text = { { "●", "GruvboxOrange" } },
			},
		},
	},
	enable_autosnippets = true,
})

ls.add_snippets("fortran", {
	s({ trig = "prim", snippetType = "autosnippet" }, {
		t("prim("),
		c(1, { t({ "rho" }), t({ "Phi" }), t({ "p" }), t({ "ux" }), t({ "uy" }) }, {}),
		t(",i,j,k) "),
		i(2),
	}),
})

ls.add_snippets("fortran", {
	s({ trig = "write", snippetType = "autosnippet" }, {
		t("write(*,*) "),
		i(1),
	}),
})
