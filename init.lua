-- TODO: would be nice to load Lsp on startup, not sure it is done rn
-- TODO: for todos, if the line is already commented, the shortcut doesn't work

-- Install lazy.nvim which is the plugin manager, this will be done automatically. See details at: https://github.com/folke/lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable", -- la stable release
		lazypath,
	})
end
vim.opt.rtp:prepend(lazypath)

-- this is cool to debug → shows the content of a lua table. Run :lua P(table)
P = function(v)
	print(vim.inspect(v))
	return v
end

--===============Basic vim options==============
-- These are basic options, I'm not sure what everything does...
--
-- This a really important option, you may choose a different mapleader (as
-- Space). It will be used for most of the sortcuts.
vim.g.mapleader = " "

vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.autoindent = true
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.smarttab = true
vim.opt.termguicolors = true
vim.opt.autoindent = true
vim.opt.smartindent = true
vim.opt.incsearch = true
vim.opt.scrolloff = 8
vim.opt.mouse = "a"
vim.opt.timeoutlen = 300
vim.opt.encoding = "utf-8"
vim.opt.compatible = false
vim.opt.splitright = true

-- this highlights the copied text for some milliseconds
local highlight_group = vim.api.nvim_create_augroup("YankHighlight", { clear = true })
vim.api.nvim_create_autocmd("TextYankPost", {
	callback = function()
		vim.highlight.on_yank({ higroup = "IncSearch", timeout = 100 })
	end,
	group = highlight_group,
	pattern = "*",
})

vim.cmd([[
syntax on
set title titlestring=
set backspace=indent,eol,start

set guifont=<Hack><12>
set guifont=Hack\ 12

filetype plugin indent on
syntax enable

"Remember folds
augroup remember_folds
autocmd!
autocmd BufWinLeave *.* mkview
autocmd BufWinEnter *.* silent! loadview
augroup END

"==============SPELL CHECKING==============
" i prefer to have it disabled by default finally.
" setlocal spell
"Type of files to exclude from spell checking (basically everything that is
"code)
autocmd FileType python,fortran,cfg,cpp,lua,rust,vim,sh setlocal nospell
set spelllang=en_us

" Control-c corrects the last mispelled word.
" typing zg while being on an unknown word will add it to the dictionary
" zw does the opposite
inoremap <C-c> <c-g>u<Esc>[s1z=`]a<c-g>u

" Toggle Transparency
let t:is_transparent = 0
function! Toggle_transparent_background()
if t:is_transparent == 0
set background=dark
let t:is_transparent = 1
else
hi Normal guibg=NONE ctermbg=NONE
let t:is_transparent = 0
endif
endfunction

let g:perl_host_prog = '/usr/bin/perl'

]])

-- THE KEYBINDINGS

--French Unicode characters: e with accents, ...
vim.keymap.set("i", "~a", "<C-v>u00e0", { desc = "à" })
vim.keymap.set("i", "~c", "<C-v>u00e7", { desc = "ç" })
vim.keymap.set("i", "~eg", "<C-v>u00e8", { desc = "è" })
vim.keymap.set("i", "~ea", "<C-v>u00e9", { desc = "é" })
vim.keymap.set("i", "~!", "<C-v>u00e9", { desc = "é" })
vim.keymap.set("i", "~ec", "<C-v>u00ea", { desc = "ê" })
vim.keymap.set("i", "~A", "<C-v>u00c0", { desc = "À" })
vim.keymap.set("i", "~C", "<C-v>u00c7", { desc = "Ç" })
vim.keymap.set("i", "~Eg", "<C-v>u00c8", { desc = "È" })
vim.keymap.set("i", "~Ea", "<C-v>u00c9", { desc = "É" })
vim.keymap.set("i", "~Ec", "<C-v>u00ca", { desc = "Ê" })
vim.keymap.set("i", "~oc", "<C-v>u00f4", { desc = "ô" })
vim.keymap.set("i", "~oe", "<C-v>u00f6", { desc = "ö" })
vim.keymap.set("i", "~u", "<C-v>u00fb", { desc = "û" })
vim.keymap.set("i", "~@", "<C-v>u00b7", { desc = "·" })
-- TODO: i trema et circonflexe
-- TODO: a circonflexe pour le mais
-- TODO: u circonflexe pour le cout et u accent grave pour ou

-- invoke Lazy
vim.keymap.set("n", "<leader>l", ":Lazy<CR>", { desc = "open [l]azy, the plugin manager", silent = true })

-- Quickly insert an empty new line without entering insert mode
vim.keymap.set("n", "<leader>o", "o<ESC>", { desc = "Add new line below" })
vim.keymap.set("n", "<leader>O", "O<ESC>", { desc = "Add new line above" })
vim.keymap.set({ "n", "v" }, "<leader>c", "<CMD>norm cc<CR>", { desc = "Empty current line" })

-- remove all the highlighting (for example after a search) with escape key
vim.keymap.set("n", "<esc>", ":noh<return><esc>", { desc = "remove highlighting", silent = true })

-- recenters some commands
vim.keymap.set("n", "<C-d>", "<C-d>zz", { desc = "recenter after <C-d>" })
vim.keymap.set("n", "<C-u>", "<C-u>zz", { desc = "recenter after <C-u>" })
vim.keymap.set("n", "{", "{zz", { desc = "recenter after {" })
vim.keymap.set("n", "}", "}zz", { desc = "recenter after }" })
vim.keymap.set("n", "n", "nzzzv", { desc = "recenter after n" })
vim.keymap.set("n", "N", "Nzzzv", { desc = "recenter after N" })

--move line(s) in visual mode with J / K
vim.keymap.set("v", "J", ":m'>+1<CR>gv=gv", { desc = "move selection up one line", silent = true })
vim.keymap.set("v", "K", ":m'<-2<CR>gv=gv", { desc = "move selection down one line", silent = true })

-- copy and paste from computer clipboard
vim.keymap.set("v", "<leader>yc", '"+y', { desc = "[Y]ank to [C]lipboard" })
vim.keymap.set({ "v", "n" }, "<leader>pc", '"+p', { desc = "[P]aste from [C]lipboard after" })
vim.keymap.set({ "v", "n" }, "<leader>Pc", '"+P', { desc = "[P]aste from [C]lipboard before" })

-- and from register a
vim.keymap.set("v", "<leader>ya", '"ay', { desc = "[Y]ank to register [a]" })
vim.keymap.set("n", "<leader>ywa", 'viw"ay', { desc = "[Y]ank [W]ord to register [a]" })
vim.keymap.set({ "n", "v" }, "<leader>pa", '"ap', { desc = "[P]aste from register [a] after" })
vim.keymap.set({ "n", "v" }, "<leader>Pa", '"aP', { desc = "[P]aste from register [a] after" })

-- Buffer action
vim.keymap.set("n", "<C-n>", ":bnext<CR>", { desc = "go to [n]ext [b]uffer", silent = true })
vim.keymap.set("n", "<C-p>", ":bprev<CR>", { desc = "go to [p]revious [b]uffer", silent = true })
vim.keymap.set("n", "<C-q>", ":bd<CR>", { desc = "[d]elete current [b]uffer", silent = true })

-- Split action
vim.keymap.set("n", "<leader>sv", ":vsplit<CR>", { desc = "open [v]ertical [s]plit", silent = true })
vim.keymap.set("n", "<leader>sh", ":split<CR>", { desc = "open [h]orizontal [s]plit", silent = true })

vim.keymap.set("t", "<Esc>", "<C-\\><C-n>", { desc = "Go out of terminal mode", silent = true })

vim.keymap.set("i", "<c-j>", "<c-o>o", { desc = "creates on line below", silent = true })
vim.keymap.set("i", "<c-k>", "<c-o>O", { desc = "creates on line above", silent = true })

-- comment current line and paste it below
vim.keymap.set(
	"n",
	"<leader>u",
	'V"by :norm gcc<CR>"bp',
	{ desc = "comment current line and paster it below", silent = true }
)

-- TODO: le problème avec ça est que ça fausse les sauts de rnu
vim.keymap.set("n", "j", "gj")
vim.keymap.set("n", "k", "gk")

-- dictionary action
vim.keymap.set("n", "<leader>Dc", ":set nospell<CR>", { desc = "dictionary close", silent = true })
vim.keymap.set("n", "<leader>Df", ":set spelllang=fr<CR>", { desc = "dictionary french", silent = true })
-- TODO: something's wrong with the dictionary source rn
-- vim.keymap.set("n", "<leader>De", ":set spelllang=en<CR>", { desc = "dictionary english", silent = true })
vim.keymap.set("n", "<leader>De", ":set spell<CR>", { desc = "dictionary english", silent = true })

vim.keymap.set(
	"n",
	"<leader>u",
	'V"by :norm gcc <CR> "bp',
	{ desc = "comment current line and copy it below ", silent = true }
)

vim.keymap.set("n", "<leader><CR>", "<CMD>so%<CR>", { desc = "source current file", silent = true })

-- vim.keymap.set("i", "->", "→", { desc = "source current file", silent = true })

vim.keymap.set("n", "<leader>te", "<CMD>EditQuery<CR>", { desc = "[t]reesitter [e]dit query", silent = true })
vim.keymap.set("n", "<leader>ti", "<CMD>InspectTree<CR>", { desc = "[t]reesitter [i]nspect tree", silent = true })

require("lazy").setup(
	{
		-- To use vifm, VI File Manager with nvim
		"vifm/vifm.vim",
		{
			"vifm/vifm.vim",
			config = function()
				vim.keymap.set("n", "<leader>v", ":Vifm<CR>", { desc = "Explore with [V]ifm", silent = true })
				vim.keymap.set("n", "<leader>ev", ":Vifm<CR>", { desc = "[E]xplore with [V]ifm", silent = true })
			end,
			event = "VeryLazy",
		},

		-- normal mode : comment line with gcc, comment block with gbc
		-- visual mode : comment line with gc, comment block with gb
		"numToStr/Comment.nvim",
		{
			"numToStr/Comment.nvim",
			event = "VeryLazy",
			config = function()
				require("Comment").setup()
			end,
		},

		-- Automatically adds the closing parenthesis, bracket ...
		"windwp/nvim-autopairs",
		{
			"windwp/nvim-autopairs",
			event = "InsertEnter",
			config = true,
		},

		-- adds targets (you can di between comma, ...) and adds possibility to cin( (next parenthesis), ci, and so on
		"wellle/targets.vim",
		{
			"wellle/targets.vim",
			keys = { "d", "v", "y", "f", "t" },
		},

		-- -- adds vertical lines to show indentation
		-- "lukas-reineke/indent-blankline.nvim",
		-- {
		-- 	"lukas-reineke/indent-blankline.nvim",
		--     main = "ibl",
		-- 	opts = {},
		-- 	-- event = "VeryLazy",
		-- },

		-- Easily surround a word with parenthesis or change / delete the surrounding ysi), cs)]
		"kylechui/nvim-surround",
		{
			"kylechui/nvim-surround",
			keys = { "y", "v", "d", "g", "c" },
			config = true,
		},

		-- Incremental fuzzy research
		-- Helps navigating easily in a file, maybe easier than easymotions
		-- triggered by s (who uses s really)
		"rlane/pounce.nvim",
		{
			"rlane/pounce.nvim",
			config = function()
				local map = vim.keymap.set
				map("n", "s", function()
					require("pounce").pounce({})
				end)
				map("x", "s", function()
					require("pounce").pounce({})
				end)
				map("o", "gs", function()
					require("pounce").pounce({})
				end)
			end,
			event = "VeryLazy",
		},

		--colorscheme
		"gruvbox-community/gruvbox",
		{
			"gruvbox-community/gruvbox",
			priority = 1000,
			config = function()
				vim.cmd.colorscheme("gruvbox")
			end,
		},

		--status bar
		"nvim-lualine/lualine.nvim",
		{
			"nvim-lualine/lualine.nvim",
			dependencies = { "nvim-tree/nvim-web-devicons", "gruvbox-community/gruvbox" },
			event = "VeryLazy",
			config = function()
				require("lualine").setup({
					options = {
						theme = "onedark",
					},
					sections = {
						lualine_b = {
							{ "branch" },
							{ "diff" },
							{
								"diagnostics",

								-- Displays diagnostics for the defined severity types
								sections = { "error", "warn", "info", "hint" },

								symbols = { error = "E", warn = "W", info = "I", hint = "H" },
								colored = true, -- Displays diagnostics status in color if set to true.
								update_in_insert = false, -- Update diagnostics in insert mode.
								always_visible = false, -- Show diagnostics even if there are none.
							},
						},
					},
				})
			end,
		},

		--to use neovim with tmux, you need to install tmux
		"aserowy/tmux.nvim",
		{
			dependencies = "alexghergh/nvim-tmux-navigation",
			keys = { "<C-h>", "<C-l>", "<C-j>", "<C-k>" },
			"aserowy/tmux.nvim",
			config = function()
				require("tmux").setup()
			end,
		},
		--to use neovim with tmux
		"alexghergh/nvim-tmux-navigation",
		{
			"alexghergh/nvim-tmux-navigation",
			lazy = true,
		},

		--LSP
		"VonHeikemen/lsp-zero.nvim",
		{
			"VonHeikemen/lsp-zero.nvim",
			branch = "v2.x",
			event = "VeryLazy",
			dependencies = {
				-- LSP Support
				{ "williamboman/mason.nvim" }, -- Optional
				{ "williamboman/mason-lspconfig.nvim", opt = { automatic_installation = true } },
				{ "neovim/nvim-lspconfig" }, -- Required
			},
			config = function()
				local lsp = require("lsp-zero")
				lsp.preset("recommended")

				-- Fix Undefined global 'vim'
				lsp.nvim_workspace()
				lsp.setup()

				vim.diagnostic.config({
					virtual_text = true,
				})
			end,
		},

		-- cool snippets stored in ~/.local/share/nvim/lazy/latex-snippets/lua/latex-snippets/
		-- fork of luasnip-latex-snippets (https://github.com/iurimateus/luasnip-latex-snippets.nvim)
		"https://gitlab.com/tgregorczyk/latex-snippets",
		{
			"https://gitlab.com/tgregorczyk/latex-snippets",
			dependencies = { "L3MON4D3/LuaSnip", "nvim-treesitter/nvim-treesitter" },
			ft = "tex",
			config = function()
				require("latex-snippets").setup({ use_treesitter = false })
			end,
		},

		-- snippet engine
		"L3MON4D3/LuaSnip",
		{
			"L3MON4D3/LuaSnip",
			-- ensures that mason* load first
			dependencies = {
				"rafamadriz/friendly-snippets",
				"williamboman/mason.nvim",
				"williamboman/mason-lspconfig.nvim",
			},
			config = function()
				local ls = require("luasnip")

				require("luasnip.loaders.from_vscode").lazy_load()
				require("luasnip.loaders.from_lua").load({ paths = "~/.config/nvim/luasnip-perso/" })
				-- Use Tab to expand and jump through snippets
				vim.keymap.set({ "i", "s" }, "<Tab>", function()
					if ls.expand_or_jumpable() then
						ls.expand_or_jump()
					end
				end, { silent = true })

				-- Use Shift-Tab to jump backwards through snippets
				vim.keymap.set({ "i", "s" }, "<S-Tab>", function()
					if ls.jumpable(-1) then
						ls.jump(-1)
					end
				end, { silent = true })

				vim.keymap.set({ "i", "s" }, "<c-l>", function()
					if ls.choice_active() then
						ls.change_choice(1)
					end
				end, { silent = true })
			end,
			lazy = true,
		},

		-- LSP / Linter / Formatter / Debugger manager
		"williamboman/mason.nvim",
		{
			"williamboman/mason.nvim",
			build = ":MasonUpdate",
			config = function()
				require("mason").setup()
			end,
			lazy = true,
		},

		--LSP manager
		"williamboman/mason-lspconfig.nvim",
		{
			"williamboman/mason-lspconfig.nvim",
			dependencies = {
				"williamboman/mason.nvim",
				"neovim/nvim-lspconfig",
			},
			config = function()
				require("mason-lspconfig").setup({
					ensure_installed = {
						"lua_ls",
						-- NOTE: to have completion for python packages (numpy, ...) you have to set
						-- include-system-site-packages = true
						-- in ~/.local/share/nvim/mason/packages/python-lsp-server/venv/pyvenv.cfg
						-- This is true for other LSPs as well.
						"pylsp",
						"fortls",
						"rust_analyzer",
						"clangd",
						"bashls",
						"texlab",
					},
				})
			end,
			lazy = true,
		},

		-- I try to lazy-load the autocompletion stuff here,
		-- after the LSPs (I removed the dependencies in lsp-zero)
		"hrsh7th/nvim-cmp",
		{
			"hrsh7th/nvim-cmp",
			dependencies = {
				{ "hrsh7th/cmp-nvim-lsp" }, -- LSP source for cmp
				{ "hrsh7th/cmp-buffer" },
				{ "hrsh7th/cmp-path" },
				{ "hrsh7th/cmp-nvim-lua" }, -- source for neovim Lua API.
				{ "hrsh7th/cmp-cmdline" },
				{ "hrsh7th/cmp-nvim-lsp-signature-help" },
				{ "saadparwaiz1/cmp_luasnip" }, -- make the link between LuaSnip and cmp
				{ "kdheepak/cmp-latex-symbols" }, -- for LaTeX
				{ "L3MON4D3/LuaSnip" }, -- snippet engine
				{ "uga-rosa/cmp-dictionary" }, --dictionary
			},
			config = function()
				local cmp = require("cmp")
				local cmp_autopairs = require("nvim-autopairs.completion.cmp")

				local function border(hl_name)
					return {
						{ "╭", hl_name },
						{ "─", hl_name },
						{ "╮", hl_name },
						{ "│", hl_name },
						{ "╯", hl_name },
						{ "─", hl_name },
						{ "╰", hl_name },
						{ "│", hl_name },
					}
				end

				local kind_icons = {
					Text = "",
					Method = "  ",
					Function = "  ",
					Constructor = "",
					Field = "  ",
					Variable = "  ",
					Class = "  ",
					Interface = "",
					Module = "",
					Property = "  ",
					Unit = "  ",
					Value = "  ",
					Enum = "  ",
					Keyword = "  ",
					Snippet = "",
					Color = "  ",
					File = "  ",
					Reference = "  ",
					Folder = "  ",
					EnumMember = "  ",
					Constant = "  ",
					Struct = "  ",
					Event = "  ",
					Operator = "  ",
					TypeParameter = "  ",
				}

				cmp.setup({
					snippet = {
						expand = function(args)
							require("luasnip").lsp_expand(args.body) -- For `luasnip` users.
						end,
					},

					performance = {
						max_view_entries = 10,
					},
					sources = { -- the order sets the priority
						{ name = "luasnip", keyword_length = 2 },
						{ name = "nvim_lsp", keyword_length = 2 },
						{ name = "nvim_lua", keyword_length = 2 },
						{ name = "buffer", keyword_length = 2 },
						{ name = "path", keyword_length = 2 },
						{ name = "nvim_lsp_signature_help", keyword_length = 2 },
						{ name = "dictionary", keyword_length = 2 },
						{ name = "latex_symbols", option = { strategy = 2 }, keyword_length = 2 }, --strategy 0 inserts the symbol not the command
					},
					mapping = cmp.mapping.preset.insert({
						["<C-p>"] = cmp.mapping.select_prev_item(),
						["<Tab>"] = cmp.mapping.select_next_item(),
						["<C-b>"] = cmp.mapping.scroll_docs(-4),
						["<C-f>"] = cmp.mapping.scroll_docs(4),
						["<C-Space>"] = cmp.mapping.complete(),
						["<C-e>"] = cmp.mapping.abort(),
						["<CR>"] = cmp.mapping.confirm({ select = true }),
					}),

					experimental = {
						ghost_text = true,
					},

					-- icons in menu
					formatting = {
						format = function(entry, vim_item)
							-- Kind icons
							vim_item.kind = string.format("%s %s", kind_icons[vim_item.kind], vim_item.kind) -- This concatonates the icons with the name of the item kind
							-- Source
							vim_item.menu = ({
								buffer = "[Buffer]",
								nvim_lsp = "[LSP]",
								luasnip = "[LuaSnip]",
								nvim_lua = "[Lua]",
								latex_symbols = "[LaTeX]",
							})[entry.source.name]
							return vim_item
						end,
					},
					window = {
						completion = {
							winhighlight = "Normal:CmpPmenu",
							scrollbar = false,
						},
						documentation = {
							border = border("CmpDocBorder"),
							winhighlight = "Normal:CmpDoc",
						},
					},
				})

				-- If you want insert `(` after select function or method item
				cmp.event:on("confirm_done", cmp_autopairs.on_confirm_done())
				cmp.setup.cmdline({ "/", "?" }, {
					mapping = cmp.mapping.preset.cmdline(),
					sources = {
						{ name = "buffer" },
					},
				})

				-- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
				cmp.setup.cmdline(":", {
					mapping = cmp.mapping.preset.cmdline(),
					sources = cmp.config.sources({
						{ name = "path" },
					}, {
						{ name = "cmdline" },
					}),
				})
			end,
			event = "VeryLazy",
		},

		-- TODO: something's wrong with the dictionary source rn
		-- --dictionary source for completion
		-- "uga-rosa/cmp-dictionary",
		-- {
		-- 	"uga-rosa/cmp-dictionary",
		-- 	config = function()
		-- 		local dict = require("cmp_dictionary")
		--
		-- 		-- dict.setup({ paths = { "/home/tgregorczyk/.config/nvim/my.dict.fr" } })
		-- 		dict.setup({})
		--
		-- 		dict.switcher({
		-- 			-- dict.paths({
		-- 			spelllang = {
		-- 				-- you need to run the following
		-- 				-- sudo apt install aspell aspell-en aspell-fr
		-- 				-- in ~/.config/nvim/
		-- 				-- aspell -d fr dump master | aspell -l fr expand > my.dict.fr
		-- 				-- aspell -d en dump master | aspell -l en expand > my.dict.en
		-- 				fr = "~/.config/nvim/my.dict.fr",
		-- 				en = "~/.config/nvim/my.dict.en",
		-- 			},
		-- 		})
		-- 	end,
		-- 	event = "VeryLazy",
		-- },

		-- Linting / Formatting
		-- null-ls is not maintained anymore, switching to conform.nvim
		-- "jose-elias-alvarez/null-ls.nvim",
		-- {
		-- 	"jose-elias-alvarez/null-ls.nvim",
		-- 	dependencies = {
		-- 		"neovim/nvim-lspconfig",
		-- 		"jay-babu/mason-null-ls.nvim",
		-- 	},
		-- 	config = function()
		-- 		local null_ls = require("null-ls")
		--
		-- 		null_ls.setup({})
		-- 		vim.keymap.set("n", "<leader>F", ":lua vim.lsp.buf.format()<CR>", { desc = "Format code if possible" })
		-- 	end,
		-- 	event = "VeryLazy",
		-- },

		--Linting / Formatting
		"stevearc/conform.nvim",
		{
			"stevearc/conform.nvim",
			config = function()
				require("conform").setup({
					formatters_by_ft = {
						lua = { "stylua" },
						-- Conform will run multiple formatters sequentially
						python = { "prettier", "black" },
						cpp = { "clang_format" },
						bash = { "beautysh", "shellcheck" },
						rust = { "rustfmt" },
						markdown = { "mdformat" },
						latex = { "latexindent" },
						fortran = { "fprettify" },
					},
					format_on_save = {
						timeout_ms = 500,
						lsp_fallback = true,
					},
				})
				-- cli argument I want to give to fprettify
				require("conform").formatters.fprettify = {
					prepend_args = { "--enable-decl", "-w", "4", "--case", "1", "1", "1", "1" },
				}
			end,
		},

		-- TODO:  i keep this for now just for the "ensured installed"
		-- maybe use an autocmd instead ?

		-- "jay-babu/mason-null-ls.nvim",
		{
			"jay-babu/mason-null-ls.nvim",
			-- event = { "BufReadPre", "BufNewFile" },
			event = "VeryLazy",
			dependencies = {
				"williamboman/mason.nvim",
				"jose-elias-alvarez/null-ls.nvim",
			},
			config = function()
				local null_ls = require("null-ls")
				-- local diagnostics = null_ls.builtins.diagnostics
				-- local code_actions = null_ls.builtins.code_actions
				-- local formatting = null_ls.builtins.formatting
				null_ls.setup()
				require("mason-null-ls").setup({
					automatic_setup = true,
					automatic_installation = true,
					ensure_installed = {
						"shellcheck", -- bash (diagnostic)
						"beautysh", -- bash (formatting)
						"black", -- python (formatting)
						-- "markdownlint", -- markdown (diagnostic and formatting)
						"mdformat", -- markdown (diagnostic and formatting)
						"clang_format", -- c, cpp, js (formatting)
						"prettier", -- many things (formatting)
						"stylua", -- lua (formatting)
						"latexindent", -- lua (formatting)
					},
					-- handlers = {
					-- 	function() end, -- disables automatic setup of all null-ls sources
					-- 	-- bash
					-- 	shellcheck = function()
					-- 		null_ls.register(diagnostics.shellcheck)
					-- 		null_ls.register(code_actions.shellcheck)
					-- 	end,
					-- 	beautysh = function()
					-- 		null_ls.register(formatting.beautysh)
					-- 	end,
					-- 	-- python
					-- 	blue = function()
					-- 		null_ls.register(formatting.blue)
					-- 	end,
					-- 	--markdown
					-- 	markdownlint = function()
					-- 		null_ls.register(diagnostics.markdownlint)
					-- 		null_ls.register(formatting.markdownlint)
					-- 	end,
					-- 	-- c, cpp, java, js
					-- 	clang_format = function()
					-- 		null_ls.register(formatting.clang_format)
					-- 	end,
					-- 	-- many things
					-- 	prettier = function()
					-- 		null_ls.register(formatting.prettier)
					-- 	end,
					-- 	-- lua
					-- 	stylua = function()
					-- 		null_ls.register(formatting.stylua)
					-- 	end,
					-- 	-- latex
					-- 	latexindent = function()
					-- 		null_ls.register(formatting.latexindent)
					-- 	end,
					-- },
				})
			end,
		},

		--shows in the status bar the git branch, the number of modified lines etc
		"lewis6991/gitsigns.nvim",
		{
			"lewis6991/gitsigns.nvim",
			opts = {
				signs = {
					add = { text = "+" },
					change = { text = "~" },
					delete = { text = "_" },
					topdelete = { text = "‾" },
					changedelete = { text = "~" },
				},
			},
			config = function()
				require("gitsigns").setup()
			end,
			event = "VeryLazy",
		},

		--colors the code, used by many plugins
		"nvim-treesitter/nvim-treesitter",
		{
			"nvim-treesitter/nvim-treesitter",
			build = ":TSUpdate",
			dependencies = { "nvim-treesitter/nvim-treesitter-context" },
			config = function()
				require("nvim-treesitter.configs").setup({
					-- ensure_installed = { "python", "lua", "rust", "latex", "cpp", "fortran", "bash" },
					ensure_installed = { "python", "lua", "rust", "cpp", "fortran", "bash" },
					ignore_install = { "latex" },
					indent = { enable = true },
					sync_install = false,
					auto_install = true,
					highlight = {
						enable = true,
						additional_vim_regex_highlighting = false,
					},
				})
			end,
			event = "VeryLazy",
		},

		-- idk
		"nvim-treesitter/nvim-treesitter-context",
		{
			"nvim-treesitter/nvim-treesitter-context",
			config = function()
				require("treesitter-context").setup()
			end,
			lazy = true,
		},

		-- complement to telescope
		"nvim-telescope/telescope-fzf-native.nvim",
		{
			"nvim-telescope/telescope-fzf-native.nvim",
			build = "cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build",
			lazy = true,
		},

		--Really cool fuzzy finder, helps finding files easily. Requires ripgrep.
		--enables lazygit directly in vim, you need to install lazygit first
		"nvim-telescope/telescope.nvim",
		{
			"nvim-telescope/telescope.nvim",
			branch = "0.1.x",
			event = "VeryLazy",
			keys = {
				--finds instances of a string in the current working directory
				{
					"<leader>fg",
					":Telescope live_grep theme=dropdown cwd=%:p:h<CR>",
					desc = "Telescope live [G]rep",
					silent = true,
				},
				--seaching in all the files is too complicated so I only search in Documents
				--This is the OG
				{
					"<leader>ff",
					":Telescope find_files theme=dropdown cwd=~/Documents<CR>",
					desc = "[F]ind [F]iles",
					silent = true,
				},
				--searches in recently opened files
				{
					"<leader>fo",
					":Telescope oldfiles theme=dropdown <CR>",
					desc = "[F]ind [O]ldfiles",
					silent = true,
				},
				-- searches in current buffers
				{
					"<leader>fb",
					":Telescope buffers",
					desc = "[F]ind in current [B]uffer",
					silent = true,
				},
				--searches through the TODOs comments that you made in cwd
				{
					"<leader>ft",
					":TodoTelescope theme=dropdown cwd=%:p:h<CR>",
					desc = "[F]ind [T]odos",
					silent = true,
				},
				-- open LazyGit, a TUI for git
				{
					"<leader>g",
					":LazyGitCurrentFile<CR>",
					desc = "Launch Lazy[G]it",
					silent = true,
				},
				{
					"<leader>fh",
					":Telescope help_tags<CR>",
					desc = "[F]ind [H]elp in doc",
					silent = true,
				},
			},
			dependencies = {
				"nvim-lua/plenary.nvim",
				"kdheepak/lazygit.nvim",
				"nvim-telescope/telescope-fzf-native.nvim",
			},
			config = function()
				require("telescope").setup()
				require("telescope").load_extension("lazygit")
				require("telescope").load_extension("fzf")
			end,
		},

		--for latex
		"lervag/vimtex",
		{
			"lervag/vimtex",
			keys = {
				{
					"<leader>T",
					":VimtexReload<CR> :VimtexCompile<CR>",
					desc = "Load Vim[T]ex and start zathura",
					silent = true,
				},
			},
			-- config = function()
			init = function()
				vim.cmd([[
		" PDF viewer (you need to install zathura)
		let g:vimtex_view_method = 'zathura'

		let g:vimtex_view_general_viewer = 'zathura'
		let g:vimtex_view_general_options = '--unique file:@pdf\#src:@line@tex'

		" VimTeX uses latexmk as the default compiler backend.
		let g:vimtex_compiler_method = 'latexmk' "latexrun

		let g:Tex_MultipleCompileformats='pdf' "for bib

		" let g:vimtex_syntax_enabled = 0
		" let g:vimtex_syntax_conceal_disable = 1

		" Most VimTeX mappings rely on localleader and this can be changed with the
		" following line. The default is usually fine and is the symbol "\".
		let maplocalleader = " "
		]])
			end,
			ft = "tex",
		},

		-- really useful to insert colored TODO, TEST, FIX, WARNING, ... in the code
		"folke/todo-comments.nvim",
		{
			"folke/todo-comments.nvim",
			keys = {
				-- NOTE: TEST doesn't work with st, use kitty or another terminal
				{ mode = "i", "~W", "WARNING: <ESC>V :norm gcc<CR>A", desc = "Insert WARNING" },
				{ mode = "i", "~TO", "TODO: <ESC>V :norm gcc<CR>A", desc = "Insert TODO" },
				{ mode = "i", "~F", "FIX: <ESC>V :norm gcc<CR>A", desc = "Insert FIX" },
				{ mode = "i", "~N", "NOTE: <ESC>V :norm gcc<CR>A", desc = "Insert NOTE" },
			},
			config = function()
				require("todo-comments").setup()
			end,
			-- event = "CursorMoved",
			event = "VeryLazy",
		},

		-- name of buffer at the top
		"akinsho/bufferline.nvim",
		{
			"akinsho/bufferline.nvim",
			version = "*",
			event = "VeryLazy",
			dependencies = { "nvim-tree/nvim-web-devicons" },
			config = function()
				require("bufferline").setup({
					options = {
						diagnostic = "nvim_lsp",
					},
				})
			end,
		},

		-- Nice welcome screen
		"goolord/alpha-nvim",
		{
			"goolord/alpha-nvim",
			event = "VimEnter",
			dependencies = { "nvim-tree/nvim-web-devicons" },
			config = function()
				require("alpha").setup(require("alpha.themes.dashboard").config)
				local alpha = require("alpha")
				local dashboard = require("alpha.themes.dashboard")

				-- Set header
				dashboard.section.header.val = {
					"                                                     ",
					"  ███╗   ██╗███████╗ ██████╗ ██╗   ██╗██╗███╗   ███╗ ",
					"  ████╗  ██║██╔════╝██╔═══██╗██║   ██║██║████╗ ████║ ",
					"  ██╔██╗ ██║█████╗  ██║   ██║██║   ██║██║██╔████╔██║ ",
					"  ██║╚██╗██║██╔══╝  ██║   ██║╚██╗ ██╔╝██║██║╚██╔╝██║ ",
					"  ██║ ╚████║███████╗╚██████╔╝ ╚████╔╝ ██║██║ ╚═╝ ██║ ",
					"  ╚═╝  ╚═══╝╚══════╝ ╚═════╝   ╚═══╝  ╚═╝╚═╝     ╚═╝ ",
					"                                                     ",
				}

				-- Set menu
				dashboard.section.buttons.val = {
					dashboard.button("e", "  > New file", ":ene <CR>"),
					dashboard.button("Space ff", "  > Find file", ":Telescope find_files cwd=~/Documents<CR>"),
					dashboard.button("Space fo", "  > Recent files", ":Telescope oldfiles<CR>"),
					dashboard.button("v", "  > Vifm", "<CMD> Vifm <CR>"),
					-- dashboard.button( "n", "  > netrw" , "<CMD> Ex <CR>"),
					dashboard.button("s", "  > Settings", ":e $MYVIMRC <CR>"),
					dashboard.button("q", "  > Quit", ":qa<CR>"),
				}

				-- Send config to alpha
				alpha.setup(dashboard.opts)

				-- Disable folding on alpha buffer
				vim.cmd([[
				autocmd FileType alpha setlocal nofoldenable
				]])
			end,
		},

		--shows the diagnostic of the code coming from the lsp
		"folke/trouble.nvim",
		{
			"folke/trouble.nvim",
			dependencies = { "nvim-tree/nvim-web-devicons" },
			keys = {
				{ "<leader>d", "<cmd>Trouble diagnostics toggle<CR>", desc = "Show [D]iagnostic" },
			},
			config = function()
				require("trouble").setup({
					signs = {
						error = "E",
						warning = "W",
						hint = "H",
						information = "I",
						other = "?",
					},
				})
			end,
			event = "VeryLazy",
		},

		--helper, shows all keybindings
		"folke/which-key.nvim",
		{
			"folke/which-key.nvim",
			keys = { "<leader>" },
			config = function()
				vim.o.timeout = true
				vim.o.timeoutlen = 300
				local wk = require("which-key")
				wk.setup({})
				wk.register({
					["<leader>D"] = {
						name = "dictionary",
					},
					["<leader>e"] = {
						name = "Explore",
					},
					["<leader>f"] = {
						name = "Find",
					},
					["<leader>h"] = {
						name = "Harpoon (open marked files)",
					},
					["<leader>p"] = {
						name = "paste",
					},
					["<leader>P"] = {
						name = "Paste",
					},
					["<leader>r"] = {
						name = "Run",
					},
					["<leader>s"] = {
						name = "Split",
					},
					["<leader>t"] = {
						name = "Terminal",
					},
					["<leader>u"] = {
						name = "Comment current line and paste it",
					},
					["<leader>y"] = {
						name = "yank",
					},
				})
			end,
		},

		-- escape with jj or jk
		"max397574/better-escape.nvim",
		{
			"max397574/better-escape.nvim",
			config = function()
				require("better_escape").setup()
			end,
			event = "InsertEnter",
		},

		-- Highlight of current line + colors on line number
		"mvllow/modes.nvim",
		{
			"mvllow/modes.nvim",
			version = "v0.2.0",
			config = function()
				require("modes").setup()
			end,
			event = "VeryLazy",
		},

		-- to use jupyter notebook with nvim // rarely used ...
		-- Open a .ju.py in nvim and Sync with a .ipynb in firefox (see doc of the plugin)
		-- "kiyoon/jupynium.nvim",
		-- {
		-- 	"kiyoon/jupynium.nvim",
		-- 	build = "pip3 install --user .",
		-- 	ft = "python",
		-- },

		-- --as the name suggests, runs code
		-- "CRAG666/code_runner.nvim",
		-- {
		-- 	"CRAG666/code_runner.nvim",
		-- 	dependencies = { "CRAG666/betterTerm.nvim" },
		-- 	config = true,
		-- 	keys = {
		-- 		{ "<leader>rf", ":RunCode<CR>", desc = "[R]un [F]ile", silent = true },
		-- 		{ "<leader>rp", ":RunProject<CR>", desc = "[R]un [P]roject", silent = true },
		-- 		{ "<leader>rc", ":RunClose<CR>", desc = "[R]un [C]lose / [C]ancel", silent = true },
		-- 	},
		-- },

		-- extend f/t command to multiple lines
		"ggandor/flit.nvim",
		{
			"ggandor/flit.nvim",
			dependencies = { "ggandor/leap.nvim", "tpope/vim-repeat" },
			config = true,
			-- event = "CursorMoved",
			event = "VeryLazy",
		},

		-- nice commande line
		-- can be used with notify (see commented lines), I personnaly prefer not to.
		"folke/noice.nvim",
		{
			"folke/noice.nvim",
			dependencies = {
				"MunifTanjim/nui.nvim",
				-- "rcarriga/nvim-notify",
			},
			event = "VeryLazy",
			config = function()
				require("noice").setup({
					lsp = {
						signature = { enabled = false },
						hover = { enabled = false },
					},
					presets = {
						bottom_search = true, -- use a classic bottom cmdline for search
						command_palette = true, -- position the cmdline and popupmenu together
						long_message_to_split = true, -- long messages will be sent to a split
						inc_rename = false, -- enables an input dialog for inc-rename.nvim
						lsp_doc_border = false, -- add a border to hover docs and signature help
					},
					--True : enables notification popup on the top RHS of the screen
					--False : usual vim message, which I prefer
					messages = {
						enabled = false,
						-- enabled = true,
					},
				})
			end,
		},

		--nice messages. Used by Mason and Lazy I think
		"rcarriga/nvim-notify",
		{
			"rcarriga/nvim-notify",
			lazy = true,
			config = function()
				require("notify").setup({
					background_colour = "#000000",
				})
			end,
		},

		-- -- embed tmux statusline into nvim statusline, but it doubles it rn
		-- {
		-- 	"vimpostor/vim-tpipeline",
		-- },

		--a nicer netrw // not used atm
		-- "prichrd/netrw.nvim",
		-- {
		-- 	"prichrd/netrw.nvim",
		-- 	config = true,
		-- 	event = "VeryLazy",
		-- 	keys = {
		-- 		{ "<leader>en", "<CMD> Ex <CR>", desc = "[E]xplore with [N]etrw" },
		-- 	},
		-- },
		--
		--sidebar tree
		--TODO: some configuration // not used atm
		-- "nvim-tree/nvim-tree.lua",
		-- {
		-- 	"nvim-tree/nvim-tree.lua",
		-- 	config = true,
		-- 	keys = {
		-- 		{ "<leader>eb", "<CMD>NvimTreeToggle <CR>", desc = "[E]xplore with side[B]ar" },
		-- 	},
		-- },
		--
		--quite a cool way to edit the cwd
		"stevearc/oil.nvim",
		{
			"stevearc/oil.nvim",
			dependencies = { "nvim-tree/nvim-web-devicons" },
			keys = {
				{ "<leader>eo", "<CMD>Oil<CR>", desc = "[E]xplore with [O]il" },
				{ "-", "<CMD>Oil<CR>", desc = "[E]xplore with [O]il" },
			},
			config = true,
		},

		-- not used atm: need to find a use case
		-- if used, configure the telescope extension
		-- "ThePrimeagen/harpoon",
		-- {
		-- 	"ThePrimeagen/harpoon",
		-- 	event = "VeryLazy",
		-- 	config = function()
		-- 		local mark = require("harpoon.mark")
		-- 		local ui = require("harpoon.ui")
		-- 		vim.keymap.set("n", "<leader>hm", mark.add_file, { desc = "[M]ark file to [H]arpoon" })
		-- 		vim.keymap.set("n", "<leader>ht", ui.toggle_quick_menu, { desc = "[T]oggle [H]arpoon menu" })
		--
		-- 		vim.keymap.set("n", "<leader>h1", function()
		-- 			ui.nav_file(1)
		-- 		end, { desc = "jump to [H]arpoon mark [1]" })
		-- 		vim.keymap.set("n", "<leader>h2", function()
		-- 			ui.nav_file(2)
		-- 		end, { desc = "jump to [H]arpoon mark [2]" })
		-- 		vim.keymap.set("n", "<leader>h3", function()
		-- 			ui.nav_file(3)
		-- 		end, { desc = "jump to [H]arpoon mark [3]" })
		-- 		vim.keymap.set("n", "<leader>h4", function()
		-- 			ui.nav_file(4)
		-- 		end, { desc = "jump to [H]arpoon mark [4]" })
		-- 		vim.keymap.set("n", "<leader>h5", function()
		-- 			ui.nav_file(5)
		-- 		end, { desc = "jump to [H]arpoon mark [5]" })
		-- 		vim.keymap.set("n", "<leader>h6", function()
		-- 			ui.nav_file(6)
		-- 		end, { desc = "jump to [H]arpoon mark [6]" })
		-- 		vim.keymap.set("n", "<leader>h7", function()
		-- 			ui.nav_file(7)
		-- 		end, { desc = "jump to [H]arpoon mark [7]" })
		-- 		vim.keymap.set("n", "<leader>h8", function()
		-- 			ui.nav_file(8)
		-- 		end, { desc = "jump to [H]arpoon mark [8]" })
		-- 		vim.keymap.set("n", "<leader>h9", function()
		-- 			ui.nav_file(9)
		-- 		end, { desc = "jump to [H]arpoon mark [9]" })
		-- 	end,
		-- },

		--helps with marks. Note that it seems like you can't really delete a mark, it will always remember the last marks.
		--You can :delm! and :delm 0-9 to delete all marks
		-- mx              Set mark x
		-- m,              Set the next available alphabetical (lowercase) mark
		-- m;              Toggle the next available mark at the current line
		-- dmx             Delete mark x
		-- dm-             Delete all marks on the current line
		-- dm<space>       Delete all marks in the current buffer
		-- m]              Move to next mark
		-- m[              Move to previous mark
		-- m:              Preview mark. This will prompt you for a specific mark to
		--                 preview; press <cr> to preview the next mark.
		--
		-- m[0-9]          Add a bookmark from bookmark group[0-9].
		-- dm[0-9]         Delete all bookmarks from bookmark group[0-9].
		-- m}              Move to the next bookmark having the same type as the bookmark under
		--                 the cursor. Works across buffers.
		-- m{              Move to the previous bookmark having the same type as the bookmark under
		--                 the cursor. Works across buffers.
		-- dm=             Delete the bookmark under the cursor.
		"chentoast/marks.nvim",
		{
			"chentoast/marks.nvim",
			config = true,
			event = "VeryLazy",
		},

		-- open a terminal
		-- to go out of terminal mode with escape key // not used atm because of tmux
		-- "akinsho/toggleterm.nvim",
		-- {
		-- 	"akinsho/toggleterm.nvim",
		-- 	version = "*",
		-- 	config = true,
		-- 	keys = {
		-- 		{
		-- 			"<leader>tf",
		-- 			"<CMD>ToggleTerm dir=%:p:h direction=float <CR>",
		-- 			desc = "Toggle floating terminal in cwd",
		-- 		},
		-- 		{
		-- 			"<leader>th",
		-- 			"<CMD>ToggleTerm dir=%:p:h direction=horizontal <CR>",
		-- 			desc = "Toggle horizontal terminal in cwd",
		-- 		},
		-- 	},
		-- },

		{
			"https://gitlab.com/tgregorczyk/subroutines.nvim",
			dependencies = { "nvim-treesitter/nvim-treesitter" },
			ft = "fortran",
		},

		{
			"lewis6991/hover.nvim",
			config = function()
				require("hover").setup({
					init = function()
						require("hover.providers.lsp")
					end,
					preview_opts = {
						border = nil,
					},
					-- Whether the contents of a currently open hover window should be moved
					-- to a :h preview-window when pressing the hover keymap.
					preview_window = false,
					title = true,
				})

				-- Setup keymaps
				vim.keymap.set("n", "K", require("hover").hover, { desc = "hover.nvim" })
				vim.keymap.set("n", "gK", require("hover").hover_select, { desc = "hover.nvim (select)" })
			end,
		},
	},

	--lazy options
	{
		-- changing default path for local plugins
		dev = { path = "~/.config/plugins" },
		performance = {
			rtp = {
				---@type string[] list any plugins you want to disable here
				disabled_plugins = {
					"gzip",
					"matchit",
					"matchparen",
					"netrwPlugin",
					"tarPlugin",
					"tohtml",
					"tutor",
					"zipPlugin",
				},
			},
		},
	}
)

local group = vim.api.nvim_create_augroup("clear", { clear = true })

-- these autocmd will overwrite the first line of any bash / python file to write the interpreter. To avoid overwriting another interpreter this happens only on files with only one line
-- I can't make BufNewFile work
vim.api.nvim_create_autocmd("FileType", {
	pattern = "sh",
	callback = function()
		local nb_lines = vim.api.nvim_buf_line_count(0)
		if nb_lines == 1 then
			vim.api.nvim_buf_set_lines(0, 0, 1, false, { "#!/usr/bin/bash" })
		end
	end,
	group = group,
})

vim.api.nvim_create_autocmd("FileType", {
	pattern = "python",
	callback = function()
		local nb_lines = vim.api.nvim_buf_line_count(0)
		if nb_lines == 1 then
			vim.api.nvim_buf_set_lines(0, 0, 1, false, { "#!/usr/bin/python3" })
		end
	end,
	group = group,
})

vim.api.nvim_create_autocmd("FileType", {
	pattern = "fortran",
	callback = function()
		vim.api.nvim_create_autocmd("BufWrite", {
			callback = function()
				require("subroutines").sub()
			end,
		})
	end,
})

--==============Background Transparency==============
vim.keymap.set(
	"n",
	"<leader>b",
	":call Toggle_transparent_background()<CR>",
	{ desc = "Toggle transparent [B]ackground" }
)

vim.cmd([[hi Normal guibg=NONE ctermbg=NONE]])
