# init.lua

## Step 0: Install Neovim

Install a recent enough version of Neovim (>0.8), see <https://github.com/neovim/neovim/wiki/Installing-Neovim>
For Ubuntu users I recommend appimage for the binary and pacstall to compile it from source.

For the appimage:
```
curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim.appimage
chmod u+x nvim.appimage
./nvim.appimage
```

To install pacstall (<https://pacstall.dev/>):

```
sudo bash -c "$(curl -fsSL https://pacstall.dev/q/install)"
```

To install neovim with pacstall:

```
pacstall -I neovim
```

## Step 1: Dependencies

Some programs needed:

- gcc, cmake, npm for some LSP
- ripgrep, bat, fzf, fd-find, lazygit for stuff like Telescope
- nerd fonts to have nice icons in the terminal
- vifm, tmux are quite nice (file manager and terminal multiplexer)
- zathura to view latex pdf

For Ubuntu (apt) users:

```
sudo apt install gcc fd-find ripgrep fzf npm cmake vifm tmux zathura bat
```

For Lazygit, see <https://github.com/jesseduffield/lazygit>:

```
LAZYGIT_VERSION=$(curl -s "https://api.github.com/repos/jesseduffield/lazygit/releases/latest" | grep -Po '"tag_name": "v\K[^"]*')
curl -Lo lazygit.tar.gz "https://github.com/jesseduffield/lazygit/releases/latest/download/lazygit_${LAZYGIT_VERSION}_Linux_x86_64.tar.gz"
tar xf lazygit.tar.gz lazygit
sudo install lazygit /usr/local/bin
```

## Now we can install this config

First backup the current config:

```
mv ~/.config/nvim/ ~/.config/nvim.bak
mv ~/.local/share/nvim/ ~/.local/share/nvim.bak
mv ~/.local/state/nvim/ ~/.local/state/nvim.bak
```

Then clone the repo:

```
git clone https://www.gitlab.com/tgregorczyk/init.lua ~/.config/nvim/ && nvim ~/.config/nvim/init.lua
```

## Uninstall

```
rm -rf ~/.config/nvim/
rm -rf ~/.local/share/nvim/
rm -rf ~/.local/state/nvim/
mv ~/.config/nvim.bak/ ~/.config/nvim
mv ~/.local/share/nvim.bak/ ~/.local/share/nvim
mv ~/.local/state/nvim.bak/ ~/.local/state/nvim
```

## Some notes
- luasnip-perso is an example directory for personnal snippets
- my.dict.fr/en are example dictionaries which are autocompletion sources. They can be created the following way:
```
sudo apt install aspell aspell-en aspell-fr
aspell -d fr dump master | aspell -l fr expand > my.dict.fr
aspell -d en dump master | aspell -l en expand > my.dict.en
```
Dictionary correction / autocompletion is usually disabled and can be managed with keybindings <leader>D or commands set spelllang ...
- It seems that with the last versions the LSP aren't showing diagnostics on screen automatically. I don't really know why but this can be fixed with a simple :LspStart <Name of the wanted LSP>
